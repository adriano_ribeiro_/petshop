<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Entity\Cliente;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     * @Template("default/index.html.twig")
     * @param SessionInterface $session
     * @return array
     */
    public function index(SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();

        $qtdAnimais = $em->getRepository(Cliente::class)->qtdAnimaisPorCliente();

        $qtdRacas = $em->getRepository(Animal::class)->qtdPorRaca();

        $cliente =  $em->getRepository(Cliente::class)->find(2);

        $session->set("nome", $cliente->getNome());

//        $nome = $session->get('nome');
//        $session->clear();
//        $session->remove($nome);

        return [
            'qtsAnimais' => $qtdAnimais,
            'qtdRaca' => $qtdRacas
//            'nome' => $nome
        ];
    }

    /**
     * @param SessionInterface $session
     *
     * @Route("pegar-sessao")
     */
    public function pegarSessao(SessionInterface $session)
    {
        echo $session->get('frase');
        exit;
    }
}
