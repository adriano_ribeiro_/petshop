<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnderecoRepository")
 */
class Endereco extends Model
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     *
     */
    private $rua;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     *
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     *
     */
    private $bairro;

    /**
     * @return string|null
     */
    public function getRua(): ?string
    {
        return $this->rua;
    }

    /**
     * @param string $rua
     * @return Endereco
     */
    public function setRua(string $rua): Endereco
    {
        $this->rua = $rua;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumero(): ?string
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     * @return Endereco
     */
    public function setNumero(string $numero): Endereco
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     * @return Endereco
     */
    public function setBairro(string $bairro): Endereco
    {
        $this->bairro = $bairro;
        return $this;
    }
}
