<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RacaRepository")
 */
class Raca extends Model
{
    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var Especie
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Especie", inversedBy="id")
     */
    private $especie;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Raca
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return Especie
     */
    public function getEspecie(): ?Especie
    {
        return $this->especie;
    }

    /**
     * @param Especie $especie
     * @return Raca
     */
    public function setEspecie(Especie $especie): Raca
    {
        $this->especie = $especie;
        return $this;
    }

    public function getNomeEspecie()
    {
        return $this->getEspecie() ? $this->getEspecie()->getNome() : null;
    }
}
