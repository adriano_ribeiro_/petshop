<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal extends Model
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    private $dataNascimento;

    /**
     * @var Cliente
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Cliente", mappedBy="animal")
     */
    private $cliente;

    /**
     * @var Raca
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Raca", inversedBy="id")
     */
    private $raca;

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Animal
     */
    public function setNome(string $nome): Animal
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    /**
     * @param \DateTime $dataNascimento
     * @return Animal
     */
    public function setDataNascimento(\DateTime $dataNascimento): Animal
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    /**
     * @return Cliente
     */
    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    /**
     * @param Cliente $cliente
     * @return Animal
     */
    public function setCliente(Cliente $cliente): Animal
    {
        $this->cliente = $cliente;
        return $this;
    }

    /**
     * @return Raca
     */
    public function getRaca(): ?Raca
    {
        return $this->raca;
    }

    /**
     * @param Raca $raca
     * @return Animal
     */
    public function setRaca(Raca $raca): Animal
    {
        $this->raca = $raca;
        return $this;
    }

}
