<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClienteRepository")
 */
class Cliente extends Model
{
    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private $telefone;

    /**
     * @ORM\Column(type="string", length=130, nullable=false)
     */
    private $email;

    /**
     * @var Endereco
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Endereco", inversedBy="id", cascade={"persist"})
     */
    private $endereco;

    /**
     * @var Animal
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Animal", inversedBy="cliente")
     * @ORM\JoinTable(name="animal_cliente")
     */
    private $animal;

    public function __construct()
    {
        $this->animal = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     * @return Cliente
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Endereco
     */
    public function getEndereco(): ?Endereco
    {
        return $this->endereco;
    }

    /**
     * @param Endereco $endereco
     * @return Cliente
     */
    public function setEndereco(Endereco $endereco): Cliente
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getAnimal(): Collection
    {
        return $this->animal;
    }

    /**
     * @param Animal $animal
     * @return Cliente
     */
    public function setAnimal(Animal $animal): Cliente
    {
        $this->animal = $animal;
        return $this;
    }
}
