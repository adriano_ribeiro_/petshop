<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EspecieRepository")
 */
class Especie extends Model
{

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Especie
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }
}
